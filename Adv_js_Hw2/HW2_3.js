// Теоретический вопрос
// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.

 // try...catch уместо использовать в случаи когда необходимо проверить, например, массив
// элементов на соотвествие условиям и сообщить об ошибке. Также поиска исключений.
//


 const books = [
                {
                    author: "Скотт Бэккер",
                    name: "Тьма, что приходит прежде",
                    price: 70
                },
                {
                    author: "Скотт Бэккер",
                    name: "Воин-пророк",
                },
                {
                    name: "Тысячекратная мысль",
                    price: 70
                },
                {
                    author: "Скотт Бэккер",
                    name: "Нечестивый Консульт",
                    price: 70
                },
                {
                    author: "Дарья Донцова",
                    name: "Детектив на диете",
                    price: 40
                },
                {
                    author: "Дарья Донцова",
                    name: "Дед Снегур и Морозочка",
                }
                ];

const container = document.getElementById('root');

books.forEach((el) => {
    const {author, name, price} = el;
    if (author && name && price) {
        container.insertAdjacentHTML("afterbegin", `<ul><li>${author}</li><li>${name}</li><li>${price}</li></ul>`);
    }
})

function check(obj) {
    const {author, name, price} = obj
    if (!author) {
        throw  new Error("No author")
    } else if (!name) {
        throw  new Error("No name")
    } else if (!price) {
        throw  new Error("No price")
    }
    return obj
}
function checkBooks(arr) {
    for (let i = 0; i < arr.length; i++) {
        try {
            check(arr[i])
        } catch (e) {
            console.error(e)
        }
    }

}

checkBooks(books)


// const div = document.getElementById('"root"');
// const ul = document.createElement('ul');
// const li = document.createElement('li');
//
//
// div.appendChild(ul);
// ul.appendChild(li);
//
// li.innerHTML = "Эта строка списка создана в JS";
// li.innerHTML = "Эта должна была быть вторая строка созданая в JS";

