// var tab; // заголовок вкладки
// var tabContent; // блок содержащий контент вкладки
//
// window.onload=function() {
//     tabContent=document.getElementsByClassName('tabContent');
//     tab=document.getElementsByClassName('tab');
//     hideTabsContent(1);
// }
// function hideTabsContent(a) {
//     for (var i=a; i<tabContent.length; i++) {
//         tabContent[i].classList.remove('show');
//         tabContent[i].classList.add("hide");
//         tab[i].classList.remove('whiteborder');
//     }
// }
// document.getElementById('tabs').onclick= function (event) {
//     var target=event.target;
//     if (target.className=='tab') {
//         for (var i=0; i<tab.length; i++) {
//             if (target == tab[i]) {
//                 showTabsContent(i);
//                 break;
//             }}}}
//
// function showTabsContent(b){
//     if (tabContent[b].classList.contains('hide')) {
//         hideTabsContent(0);
//         tab[b].classList.add('whiteborder');
//         tabContent[b].classList.remove('hide');
//         tabContent[b].classList.add('show');
//     }
// }


//ВТОРОЙ ВАРИАНТ. ПОКА НЕ РАБОТАЕТ

//  document.addEventListener('DOMContentLoaded', function(){
//     'use strict';
//
//     var doc = document,
//         tabNavList = doc.querySelectorAll('#tabNav a'),
//         tabs = doc.querySelectorAll('.tab');
//
//     var removeClass = function(elem, className){
//         var elemLength = elem.length,
//             i;
//
//         // Будем использовать цикл, когда элементов в коллекции будет больше 1
//         if(elemLength > 1){
//             for (i = 0; i < elemLength; i++) {
//                 elem[i].classList.remove(className);
//             }
//         } else {
//             elem.classList.remove(className);
//         }
//     };
//
//     for (var i = 0; i < tabNavList.length; i++) {
//         tabNavList[i].addEventListener('click', function(event){
//             event.preventDefault();
//
//             // Храним id вкладки
//             var tabID = doc.getElementById(this.getAttribute('href').slice(1));
//
//             // Активный класс у ссылок
//             removeClass(tabNavList, 'act');
//             this.classList.add('act');
//
//             // Активный класс у вкладок
//             removeClass(tabs, 'act');
//             tabID.classList.add('act');
//         });
//     };
// });
// window.addEventListener('DOMContentLoaded', function() {
//
//     'use strict';
//
//     let tab = document.querySelectorAll('.title_header_tabs'),
//         info = document.querySelector('.tabs_header'),
//         content = document.querySelectorAll('.tabContent');
//
//     function hideTabContent(a) {
//         for (let i = a; i < content.length; i++) {
//             content[i].classList.remove('show');
//             content[i].classList.add('hide');
//             tab[i].classList.remove('active');
//         }
//     }
//
//     hideTabContent(1);
//
//     function showTabContent(b) {
//         if(content[b].classList.contains('hide')) {
//             content[b].classList.remove('hide');
//             content[b].classList.add('show');
//             tab[b].classList.add('active');
//         }
//     }
//
//     info.addEventListener('click', function(event) {
//         let target = event.target;
//         if (target && target.classList.contains('title_header_tabs')) {
//             for(let i = 0; i < tab.length; i++) {
//                 if (target == tab[i]) {
//                     hideTabContent(0);
//                     showTabContent(i);
//                     break;
//                 }
//             }
//         }
//     });
//
// });


const tabs = document.querySelectorAll('.tabs-title');
const tabsContentItems = document.querySelectorAll(".tabs-content-items")

tabs.forEach(elem => {

    elem.addEventListener('click', () => {
        let tabID = elem.getAttribute("data-tab");
        let currentTab = document.querySelector(tabID);

        tabs.forEach((e => { e.classList.remove('active'); }));
        tabsContentItems.forEach(elem => {
            elem.classList.remove('active'); });
        elem.classList.add('active');
        currentTab.classList.add('active');

    })
})



